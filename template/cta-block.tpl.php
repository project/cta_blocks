<a href="<?php print url($link); ?>" class="cta-link <?php print $machine_name; ?>">
  <h4><?php print $title; ?></h4>
  <?php if ($subtitle): ?>
    <p class="subtitle"><?php print $subtitle; ?></p>
  <?php endif; ?>
</a>
